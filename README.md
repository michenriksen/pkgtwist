# pkgtwist

Pkgtwist is a tool for finding potential [typosquattings](https://en.wikipedia.org/wiki/Typosquatting) of Go packages and modules hosted on GitHub and GitLab.

## How it works

When pkgtwist is given one or more Go package import paths, e.g. `github.com/stretchr/testify`, it will generate potential mistypings, or permutations, of the package owner using various strategies to check if potential typosquatting variations of the owner exists on the platform. If it finds a  potential typosquat, it then proceeds to check if the typosquat owner has a repository with the same name as the original package.

### Typosquat permutations

These are the typosquat permutation strategies that pkgtwist currently uses:

 - **Omission:** removal of a single character (missing a key press, `stretchr` => `strechr`)
 - **Repetition:** repetition of characters (pressing a key twice, `stretchr` => `stretchhr`)
 - **[Bitsquatting](https://en.wikipedia.org/wiki/Bitsquatting):** possible bit-flip errors (`stretchr` => `strftchr`)
 - **Transposition:** swapping of adjacent characters (pressing keys in the wrong order, `stretchr` => `strethcr`)

If pkgtwist is given `github.com/stretchr/testify` as input, it will check for the existence of the following typosquat variations of `stretchr` on GitHub:

`tretchr` `sretchr` `stetchr` `strtchr` `strechr` `strethr` `stretcr` `stretch` `sstretchr` `sttretchr` `strretchr` `streetchr` `strettchr` `stretcchr` `stretchhr` `stretchrr` `rtretchr` `qtretchr` `ptretchr` `wtretchr` `vtretchr` `utretchr` `ttretchr` `suretchr` `svretchr` `swretchr` `spretchr` `sqretchr` `srretchr` `ssretchr` `stsetchr` `stpetchr` `stqetchr` `stvetchr` `stwetchr` `sttetchr` `stuetchr` `strdtchr` `strgtchr` `strftchr` `stratchr` `strctchr` `strbtchr` `streuchr` `strevchr` `strewchr` `strepchr` `streqchr` `strerchr` `streschr` `stretbhr` `stretahr` `stretghr` `stretfhr` `stretehr` `stretdhr` `stretcir` `stretcjr` `stretckr` `stretclr` `stretcmr` `stretcnr` `stretcor` `stretchs` `stretchp` `stretchq` `stretchv` `stretchw` `stretcht` `stretchu` `tsretchr` `srtetchr` `stertchr` `strtechr` `strecthr` `strethcr` `stretcrh`

## Installing pkgtwist

Run `go get gitlab.com/michenriksen/pkgtwist/cmd/pkgtwist` (`go install ...` if using Go 1.16) in a terminal.

## Using pkgtwist

Pkgtwist needs to be able to query both the github.com and gitlab.com API to check for typosquat repositories and projects. Therefore, you will need to have a user on both platforms and generate personal access tokens:

- [How to generate personal access token on GitHub](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token)
- [How to generate personal access token on GitLab](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)

These personal access tokens need to be registered as environment variables for your shell:

```sh
export PKGTWIST_GITHUB_ACCESS_TOKEN=<GITHUB_ACCESS_TOKEN>
export PKGTWIST_GITLAB_ACCESS_TOKEN=<GITLAB_ACCESS_TOKEN>
```

Alternatively, you give these tokens as command line options to pkgtwist, but that is not recommended as the tokens will be saved in your command history.

Now simply pipe a list of go package import paths to pkgtwist and it will start checking for typosquat variations:

```bash
$ cat packages.txt | pkgtwist
```

See `pkgtwist -h` for additional command line options.

