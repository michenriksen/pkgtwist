package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/google/go-github/v33/github"
	"github.com/rs/zerolog"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/michenriksen/pkgtwist/internal"
	"gitlab.com/michenriksen/pkgtwist/internal/service"
)

var ctx = context.Background()

func main() {
	opts, output, err := parseOptions(os.Args[0], os.Args[1:])
	if err == flag.ErrHelp {
		fmt.Println(output)
		os.Exit(1)
	} else if err != nil {
		fmt.Println("options parsing error: ", err)
		fmt.Println("output:\n", output)
		os.Exit(1)
	}

	logger := initLogger(opts)
	logger.Info().Msgf("Started pkgtwist at %s", time.Now().Format(time.RFC3339))

	githubService, err := service.Github(ctx, opts.githubAccessToken)
	if err != nil {
		logger.Fatal().Err(err).Msg("Error when creating GitHub service")
	}
	gitlabService, err := service.Gitlab(ctx, opts.gitlabAccessToken)
	if err != nil {
		logger.Fatal().Err(err).Msg("Error when creating GitLab service")
	}

	pkgsByOwner := make(map[string][]internal.Package)
	pkgSeenMap := make(map[string]struct{})

	reader := bufio.NewReader(os.Stdin)
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		line = strings.TrimSpace(strings.TrimPrefix(line, "https://"))
		if _, seen := pkgSeenMap[line]; seen {
			continue
		}
		pkg, err := internal.NewPackage(line)
		if err == internal.ErrUnsupportedPkgPath {
			logger.Warn().Msgf("Package path '%s' is unsupported", line)
			continue
		}
		if err != nil {
			logger.Error().Err(err).Msgf("Error when parsing '%s'", line)
			continue
		}
		pkgSeenMap[line] = struct{}{}
		if pkgs, seen := pkgsByOwner[pkg.Owner]; seen {
			pkgsByOwner[pkg.Owner] = append(pkgs, pkg)
			continue
		}
		pkgsByOwner[pkg.Owner] = []internal.Package{pkg}
	}

	total := len(pkgsByOwner)
	current := 0
	for _, pkgs := range pkgsByOwner {
		progress := calcProgress(current, total)
		current++
		if pkgs[0].Host == internal.PackageHostGithub {
			checkGithubPackages(pkgs, &githubService, logger.With().Str("progress", progress).Logger())
			continue
		}
		checkGitlabPackages(pkgs, &gitlabService, logger.With().Str("progress", progress).Logger())
	}
}

func checkGithubPackages(pkgs []internal.Package, githubService *service.GithubService, logger zerolog.Logger) {
	pkg := pkgs[0]
	logger = logger.With().Str("orig_owner", pkg.Owner).Logger()
	logger.Info().Msgf("Checking for potential typosquats of owner %s/%s", pkg.Host, pkg.Owner)
	permutations, err := internal.Permute(pkg.Owner)
	if err != nil {
		logger.Error().Err(err).Msgf("Error when permuting owner package %s", pkg.Owner)
		return
	}
	for _, permutation := range permutations {
		pLogger := logger.With().Str("permutation", permutation.Strategy).Str("typosquat_owner", permutation.Result).Logger()
		typosquatOwner, err := githubService.GetOwner(permutation.Result)
		if err == service.ErrGithubOwnerNotFound {
			pLogger.Info().Msgf("%s/%s does not exist", pkg.Host, permutation.Result)
			continue
		}
		if err != nil {
			pLogger.Error().Err(err).Msgf("Error when getting owner %s from GitHub", permutation.Result)
		}
		pLogger.Warn().
			Str("event", "typosquat_owner").
			Str("url", typosquatOwner.GetHTMLURL()).
			Msgf("Potential typosquat owner %s/%s exists", pkg.Host, permutation.Result)
		var repos []*github.Repository
		if typosquatOwner.GetType() == "User" {
			repos, err = githubService.GetUserRepos(permutation.Result)
		} else {
			repos, err = githubService.GetOrgRepos(permutation.Result)
		}
		if err != nil {
			pLogger.Error().Err(err).Msgf("Error when retrieving repositories for %s/%s", pkg.Host, permutation.Result)
			continue
		}
		for _, repo := range repos {
			if repo.GetLanguage() != "Go" {
				continue
			}
			for _, pkg := range pkgs {
				if repo.GetName() == pkg.Name {
					pLogger.Warn().
						Str("event", "typosquat_package").
						Str("url", repo.GetHTMLURL()).
						Str("typosquat_name", repo.GetName()).
						Str("typosquat_repo", fmt.Sprintf("%s/%s", pkg.Host, repo.GetFullName())).
						Str("orig_name", pkg.Name).
						Str("orig_repo", pkg.Repo()).
						Str("orig_path", pkg.Path).
						Msgf("Potential typosquat of %s: %s/%s", pkg.Path, pkg.Host, repo.GetFullName())
				}
			}
		}
	}
}

func checkGitlabPackages(pkgs []internal.Package, gitlabService *service.GitlabService, logger zerolog.Logger) {
	pkg := pkgs[0]
	logger = logger.With().
		Str("orig_owner", pkg.Owner).
		Logger()

	logger.Info().Msgf("Checking for potential typosquats of owner %s/%s", pkg.Host, pkg.Owner)
	permutations, err := internal.Permute(pkg.Owner)
	if err != nil {
		logger.Error().Err(err).Msgf("Error when permuting owner package %s", pkg.Owner)
		return
	}
	for _, permutation := range permutations {
		pLogger := logger.With().Str("permutation", permutation.Strategy).Str("typosquat_owner", permutation.Result).Logger()
		typosquatUser, err := gitlabService.GetUser(permutation.Result)
		if err != nil && err != service.ErrGitlabUserNotFound {
			pLogger.Error().Err(err).Msgf("Error when getting user %s from GitLab", permutation.Result)
		}
		typosquatGroup, err := gitlabService.GetGroup(permutation.Result)
		if err != nil && err != service.ErrGitlabGroupNotFound {
			pLogger.Error().Err(err).Msgf("Error when getting group %s from GitLab", permutation.Result)
		}
		if typosquatUser == nil && typosquatGroup == nil {
			pLogger.Info().Msgf("%s/%s does not exist", pkg.Host, permutation.Result)
			continue
		}
		var projects []*gitlab.Project
		if typosquatUser != nil {
			pLogger.Warn().
				Str("event", "typosquat_owner").
				Str("url", typosquatUser.WebURL).
				Msgf("Potential typosquat user %s/%s exists", pkg.Host, permutation.Result)
			projects, err = gitlabService.GetUserProjects(typosquatUser.ID)
		} else {
			pLogger.Warn().
				Str("event", "typosquat_owner").
				Str("url", typosquatUser.WebURL).
				Msgf("Potential typosquat group %s/%s exists", pkg.Host, permutation.Result)
			projects, err = gitlabService.GetGroupProjects(typosquatGroup.ID)
		}
		if err != nil {
			pLogger.Error().Err(err).Msgf("Error when retrieving projects for %s/%s", pkg.Host, permutation.Result)
			continue
		}
		for _, project := range projects {
			for _, pkg := range pkgs {
				if project.Path == pkg.Name {
					pLogger.Warn().
						Str("event", "typosquat_package").
						Str("url", project.WebURL).
						Str("typosquat_name", project.Path).
						Str("typosquat_repo", fmt.Sprintf("%s/%s", pkg.Host, project.PathWithNamespace)).
						Str("orig_name", pkg.Name).
						Str("orig_repo", pkg.Repo()).
						Str("orig_path", pkg.Path).
						Msgf("Potential typosquat of %s: %s/%s", pkg.Path, pkg.Host, project.PathWithNamespace)
				}
			}
		}
	}
}

func initLogger(o options) zerolog.Logger {
	var out io.Writer
	if o.outfile != "" {
		f, err := os.Create(o.outfile)
		if err != nil {
			fmt.Printf("Error when creating out file: %s\n", err)
			os.Exit(1)
		}
		console := zerolog.NewConsoleWriter(
			func(w *zerolog.ConsoleWriter) {
				w.TimeFormat = "15:04:05"
			},
		)
		out = zerolog.MultiLevelWriter(console, f)
	} else {
		out = zerolog.NewConsoleWriter(
			func(w *zerolog.ConsoleWriter) {
				w.TimeFormat = "15:04:05"
			},
		)
	}
	logger := zerolog.New(out).With().Timestamp().Logger()
	logger.Level(zerolog.WarnLevel)
	if o.verbose {
		logger.Level(zerolog.InfoLevel)
	}
	return logger
}

func calcProgress(current, total int) string {
	if current >= total {
		return "100.00"
	}
	p := (float32(current) * float32(100)) / float32(total)
	return fmt.Sprintf("%.2f", p)
}
