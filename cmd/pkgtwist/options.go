package main

import (
	"bytes"
	"flag"
	"os"
)

const (
	githubAccessTokenEnvVar = "PKGTWIST_GITHUB_ACCESS_TOKEN"
	gitlabAccessTokenEnvVar = "PKGTWIST_GITLAB_ACCESS_TOKEN"
)

type options struct {
	githubAccessToken string
	gitlabAccessToken string
	outfile           string
	outputJSON        bool
	verbose           bool
}

func parseOptions(progname string, args []string) (options, string, error) {
	flags := flag.NewFlagSet(progname, flag.ContinueOnError)
	var buf bytes.Buffer
	flags.SetOutput(&buf)

	var opts options
	flags.StringVar(&opts.githubAccessToken, "github-access-token", "", "Access token for GitHub API (default "+githubAccessTokenEnvVar+" env variable)")
	flags.StringVar(&opts.gitlabAccessToken, "gitlab-access-token", "", "Access token for GitLab API (default "+gitlabAccessTokenEnvVar+" env variable)")
	flags.StringVar(&opts.outfile, "out", "", "Write output to file and STDOUT")
	flags.BoolVar(&opts.outputJSON, "json", false, "Write output in JSON format")
	flags.BoolVar(&opts.verbose, "verbose", false, "Enable versbose output")

	err := flags.Parse(args)
	if err != nil {
		return options{}, buf.String(), err
	}

	if opts.githubAccessToken == "" {
		opts.githubAccessToken = os.Getenv(githubAccessTokenEnvVar)
	}
	if opts.gitlabAccessToken == "" {
		opts.gitlabAccessToken = os.Getenv(gitlabAccessTokenEnvVar)
	}

	return opts, buf.String(), nil
}
