module gitlab.com/michenriksen/pkgtwist

go 1.16

require (
	github.com/google/go-github/v33 v33.0.0
	github.com/michenriksen/typogenerator v0.2.1
	github.com/rs/zerolog v1.20.0
	github.com/xanzy/go-gitlab v0.44.0
	golang.org/x/oauth2 v0.0.0-20181106182150-f42d05182288
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba
)
