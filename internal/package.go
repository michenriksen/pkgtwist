package internal

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

// PackageHost represents a Go package host.
type PackageHost string

var (
	// ErrUnsupportedPkgPath is returned when a package path is not supported by pkgtwist.
	ErrUnsupportedPkgPath = errors.New("package path is not supported")

	// PackageHostGithub represents GitHub as a package host.
	PackageHostGithub PackageHost = "github.com"
	// PackageHostGitlab represents GitLab as a package host.
	PackageHostGitlab PackageHost = "gitlab.com"

	// GithubPkgPathRegexp matches path for a Go package hosted on GitHub.
	GithubPkgPathRegexp = regexp.MustCompile(`^github\.com\/[A-Za-z0-9-]{1,39}\/[A-Za-z0-9\._-]{1,100}\/?`)
	// GitlabPkgPathRegexp matches path for a Go package hosted on GitLab.
	GitlabPkgPathRegexp = regexp.MustCompile(`^gitlab\.com\/(?:[a-zA-Z0-9\._-]{2,255})\/[a-zA-Z0-9\._-]{2,255}\/?`)
)

// Package represents a Go package or module.
type Package struct {
	Path  string
	Owner string
	Name  string
	Host  PackageHost
}

// NewPackage constructs a new Pkg
func NewPackage(path string) (Package, error) {
	var host PackageHost
	if GithubPkgPathRegexp.MatchString(path) {
		host = PackageHostGithub
	} else if GitlabPkgPathRegexp.MatchString(path) {
		host = PackageHostGitlab
	} else {
		return Package{}, ErrUnsupportedPkgPath
	}
	split := strings.Split(path, "/")
	return Package{
		Path:  path,
		Owner: split[1],
		Name:  split[2],
		Host:  host,
	}, nil
}

func (p *Package) Repo() string {
	return fmt.Sprintf("%s/%s/%s", p.Host, p.Owner, p.Name)
}
