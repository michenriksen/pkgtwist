package internal

import (
	"strings"

	"github.com/michenriksen/typogenerator"
	"github.com/michenriksen/typogenerator/strategy"
)

// Permutation represents a string permutated with a specific permutation strategy.
type Permutation struct {
	Result, Strategy string
}

var permutationStrategies = []strategy.Strategy{
	strategy.Omission,
	strategy.Repetition,
	strategy.BitSquatting,
	strategy.Transposition,
}

// Permute permutates a string with a collection of permutation strategies and returns the results.
func Permute(s string) ([]Permutation, error) {
	var permutations []Permutation
	results, err := typogenerator.Fuzz(s, permutationStrategies...)
	if err != nil {
		return permutations, err
	}
	for _, result := range results {
		for _, permutation := range result.Permutations {
			if strings.ToLower(permutation) == strings.ToLower(s) {
				continue
			}
			permutations = append(permutations, Permutation{Result: permutation, Strategy: strings.ToLower(result.StrategyName)})
		}
	}
	return permutations, nil
}
