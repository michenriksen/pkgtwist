package service

import (
	"context"
	"errors"
	"time"

	"github.com/google/go-github/v33/github"
	"golang.org/x/oauth2"
	"golang.org/x/time/rate"
)

const requestsPerHour = 5000

var (
	// ErrGithubOwnerNotFound is returned when an owner is not found on GitHub.
	ErrGithubOwnerNotFound = errors.New("owner not found")
)

// GithubService is a service to fetch resources from the GitHub API.
type GithubService struct {
	client *github.Client
	ctx    context.Context
	rl     *rate.Limiter
}

// Github returns a new GitHub service configured with the given access token.
func Github(ctx context.Context, token string) (GithubService, error) {
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)
	tc := oauth2.NewClient(ctx, ts)
	client := github.NewClient(tc)
	rl := rate.NewLimiter(rate.Every(1*time.Hour/requestsPerHour), 1)
	return GithubService{
		client: client,
		ctx:    ctx,
		rl:     rl,
	}, nil
}

// GetOwner returns the user or organization with the given name.
func (s *GithubService) GetOwner(login string) (*github.User, error) {
	if err := s.rl.Wait(s.ctx); err != nil {
		return nil, err
	}
	user, resp, err := s.client.Users.Get(s.ctx, login)
	if resp.StatusCode == 404 {
		return nil, ErrGithubOwnerNotFound
	}
	return user, err
}

// GetUserRepos gets all GitHub repositories belonging to a user with the given username.
// Pagination is handled if needed.
func (s *GithubService) GetUserRepos(user string) ([]*github.Repository, error) {
	var repos []*github.Repository
	opts := &github.RepositoryListOptions{
		ListOptions: github.ListOptions{PerPage: 100},
	}
	for {
		if err := s.rl.Wait(s.ctx); err != nil {
			return nil, err
		}
		rr, resp, err := s.client.Repositories.List(s.ctx, user, opts)
		if err != nil {
			return nil, err
		}
		repos = append(repos, rr...)
		if resp.NextPage == 0 {
			break
		}
		opts.Page = resp.NextPage
	}
	return repos, nil
}

// GetOrgRepos gets all GitHub repositories belonging to an organization with the given name.
// Pagination is handled if needed.
func (s *GithubService) GetOrgRepos(org string) ([]*github.Repository, error) {
	var repos []*github.Repository
	opts := &github.RepositoryListByOrgOptions{
		ListOptions: github.ListOptions{PerPage: 100},
	}
	for {
		rr, resp, err := s.client.Repositories.ListByOrg(s.ctx, org, opts)
		if err != nil {
			return nil, err
		}
		repos = append(repos, rr...)
		if resp.NextPage == 0 {
			break
		}
		opts.Page = resp.NextPage
	}
	return repos, nil
}
