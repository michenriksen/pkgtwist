package service

import (
	"context"
	"errors"
	"time"

	"github.com/xanzy/go-gitlab"
	"golang.org/x/time/rate"
)

const requestsPerMinute = 2000

var (
	// ErrGitlabUserNotFound is returned when a user is not found on GitLab.
	ErrGitlabUserNotFound = errors.New("user not found")
	// ErrGitlabGroupNotFound is returned when a group is not found on GitLab.
	ErrGitlabGroupNotFound = errors.New("user not found")
)

// GitlabService is a service to fetch resources from the GitLab API.
type GitlabService struct {
	client *gitlab.Client
	ctx    context.Context
	rl     *rate.Limiter
}

// Gitlab returns a new GitLab service configured with the given access token.
func Gitlab(ctx context.Context, token string) (GitlabService, error) {
	client, err := gitlab.NewClient(token)
	if err != nil {
		return GitlabService{}, err
	}
	rl := rate.NewLimiter(rate.Every(1*time.Minute/requestsPerMinute), 1)
	return GitlabService{
		client: client,
		ctx:    ctx,
		rl:     rl,
	}, nil
}

// GetUser gets a GitLab User with the given username.
func (s *GitlabService) GetUser(username string) (*gitlab.User, error) {
	if err := s.rl.Wait(s.ctx); err != nil {
		return nil, err
	}
	uu, resp, err := s.client.Users.ListUsers(&gitlab.ListUsersOptions{Username: &username})
	if resp.StatusCode == 404 {
		return nil, ErrGitlabUserNotFound
	}
	if err != nil {
		return nil, err
	}
	if len(uu) != 1 {
		return nil, ErrGitlabUserNotFound
	}
	return uu[0], nil
}

// GetGroup gets a GitLab Group with the given ID or path.
func (s *GitlabService) GetGroup(gid interface{}) (*gitlab.Group, error) {
	if err := s.rl.Wait(s.ctx); err != nil {
		return nil, err
	}
	g, resp, err := s.client.Groups.GetGroup(gid)
	if resp.StatusCode == 404 {
		return nil, ErrGitlabGroupNotFound
	}
	return g, err
}

// GetGroupProjects gets all projects belonging to a group with the given ID or path.
// Pagination is handled if needed.
func (s *GitlabService) GetGroupProjects(gid interface{}) ([]*gitlab.Project, error) {
	var projects []*gitlab.Project
	withShared := false
	opts := &gitlab.ListGroupProjectsOptions{
		WithShared: &withShared,
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}
	for {
		if err := s.rl.Wait(s.ctx); err != nil {
			return nil, err
		}
		pp, resp, err := s.client.Groups.ListGroupProjects(gid, opts)
		if err != nil {
			return nil, err
		}
		projects = append(projects, pp...)
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opts.Page = resp.NextPage
	}
	return projects, nil
}

// GetUserProjects gets all GitLab Projects belonging to a user with the given ID or username.
// Pagination is handled if needed.
func (s *GitlabService) GetUserProjects(uid interface{}) ([]*gitlab.Project, error) {
	var projects []*gitlab.Project
	opts := &gitlab.ListProjectsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}
	for {
		if err := s.rl.Wait(s.ctx); err != nil {
			return nil, err
		}
		pp, resp, err := s.client.Projects.ListUserProjects(uid, opts)
		if err != nil {
			return nil, err
		}
		projects = append(projects, pp...)
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opts.Page = resp.NextPage
	}
	return projects, nil
}
